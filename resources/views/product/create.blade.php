<!-- resources/views/product/create.blade.php -->

@extends('layouts/template') 
 
@section('content') 
 
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif  
<div class="container">
    <div class="row">
        <div class="col-md-8 ">
            <form method="post" action="{{action('ProductController@store')}}"> 
              {{ csrf_field() }} 
              <div class="panel panel-default">
                    <div class="panel-heading">
                     <h4 class="pull-left">
                           Create a Product
                    </h4>
                    <button class="pull-right"><a href="{{ url('product')}}" class="btn btn-default" >Cancel</a></button>
                    <button class="pull-right">{!! Form::submit('Create', array('class' => 'btn btn-default')) !!}</button>
                    <div class="clearfix"></div>
                    </div>
                  
            <div class="panel-body">
                
        <div class="form-group">  
        <label for="Name">Name:</label>
        <div class="col-6">
        <input type="text" name="Name" value="{{ old('Name') }}"  class="form-control" /> 
        </div></div> 
         
       <div class="form-group"> 
        <label for="Description">Description:</label>
        <div class="col-6">
        <textarea class="form-control" rows="2" name="Description" value="{{ old('Description') }}" ></textarea>
        </div></div> 
        
        <div class="form-group"> 
        <label for="Price">Price:</label>
        <div class="col-6">
        <input type="text" name="Price" value="{{ old('Price') }}" class="form-control" /> 
        </div></div> 
         
        <div class="form-group"> 
        <label for="ShippingCost">ShippingCost:</label>
        <div class="col-6">
        <input type="text" name="ShippingCost" value="{{ old('ShippingCost') }}"  class="form-control" /> 
        </div></div> 
        
        <div class="form-group">  
        <label for="TotalRating">TotalRating:</label>
        <div class="col-6">
        <input type="text" name="TotalRating" value="{{ old('TotalRating') }}"  class="form-control" /> 
        </div></div> 
        
        <div class="form-group">  
        <label for="Thumbnail">Thumbnail:</label>
        <div class="col-6">
        <input type="text" name="Thumbnail" value="{{ old('Thumbnail') }}"  class="form-control" /> 
        </div></div> 
        
        <div class="form-group">  
        <label for="Image">Image:</label>
        <div class="col-6">
        <input type="text" name="Image" value="{{ old('Image') }}" class="form-control" /> 
        </div></div> 
        
        <div class="form-group"> 
        <label for="DiscountPercentage">DiscountPercentage:</label>
        <div class="col-6">
        <input type="text" name="DiscountPercentage" value="{{ old('DiscountPercentage') }}"  class="form-control" /> 
        </div></div> 
        
        <div class="form-group">  
        <label for="Votes">Votes:</label>
        <div class="col-6">
        <input type="text" name="Votes" value="{{ old('Votes') }}"  class="form-control" /> 
        </div></div> 
         
       <div class="form-group"> 
        {!! Form::Label('IdCategory', 'IdCategory:') !!}
        <div class="col-6">
        <select class="form-control form-control-lg" name="IdCategory"> 
            @foreach($categories as $Category) 
             <option value="{{$Category->id}}">{{$Category->Name}}</option>
            @endforeach 
        </select></div> 
        </div>
        

          </div> <!--panel-body-->
        </div>
    </form>
</div>
 <!--Table on Right-->
        <div class="col-md-4">
            </br>
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Select</th>
                    <th>ID</th>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                     @foreach($products as $key => $value)
                    <tr>
                        <td align="center" ><a href="/product/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a></td>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->Name }}</td>
                    </tr>

                    @endforeach
            </table>
        </div>
    </div>
@stop