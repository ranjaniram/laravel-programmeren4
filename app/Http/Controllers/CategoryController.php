<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Category;

class CategoryController extends Controller
{
    
    public function index()
    {
        $categories = Category::all();
        return view('category.index', array('categories'=>$categories));
    }

    
    public function create()
    {
        $categories = Category::all();
        return view('category.create',array('categories' => $categories));
    }

    
    public function store(Request $request)
    {
        //use Validator class
         $validator = Validator::make($request->all(), [ 
        'Name' => 'required|unique:categories|max:255', 
        'Description' => 'max:1024', 
        ]);
        
        if ($validator->fails()) { 
            return redirect('category/create') 
                        ->withErrors($validator) 
                        ->withInput($request->all()); 
        } 
        
        
       $category =new Category;
        $category->Name = $request->input('Name');
        $category->Description = $request->input('Description');
        $category->save();
        
        Session::flash('message', 'Successfully created category!'); 
        return redirect('/category');
    }

    
    public function show($id)
    {
        $category = Category::find($id);
        $categories = Category::all();
        return view('category.show',array('category' => $category,'categories' => $categories));
    }

   
    public function edit($id)
    {
        $category = Category::find($id);
        $categories = Category::all();
        return view('category.edit',array('category' => $category,'categories' => $categories));
    }
    

    
    public function update(Request $request, $id)
    {
        //use Validator class
         $validator = Validator::make($request->all(), [ 
        'Name' => 'required|unique:categories, id, '.$id.'|max:255', 
        'Description' => 'max:1024', 
        ]);
        
        if ($validator->fails()) { 
            return redirect('category/'.$id.'/edit') 
                        ->withErrors($validator) 
                        ->withInput($request->all()); 
        } 
        
        $category = Category::find($id);
        $category->Name = $request->input('Name');
        $category->Description = $request->input('Description');
        $category->save();
        
        Session::flash('message', 'Successfully updated category!'); 
        return redirect('/category');
        
    }

    
    public function destroy($id)
    {
        Category::find($id)->delete();
        return redirect('/category');
    }
}
