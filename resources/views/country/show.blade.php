<!-- resources/views/country/show.blade.php -->

@extends('layouts/template')

@section('content')

  <div class="container">
    <div class="row"> 
     <div class="col-md-8"> 
        <div class="panel panel-default">
        <div class="panel-heading">
        <h4 class="pull-left">Country : {{ $country->name }}</h4>
        <a href="{{ url('country')}}" class="btn btn-default pull-right" >CANCEL</a>
        <form method="POST" action="{{ action('CountryController@destroy', $country->id) }}"> 
                            <input type="hidden" name="_method" value="DELETE"/>     
                            {{ csrf_field() }} 
                             <button class="btn btn-default pull-right">DELETE</button> 
                             </form>
        <form method="GET" action="{{ action('CountryController@edit', $country->id) }}"> 
                            <button class="btn btn-default pull-right">UPDATING</button> 
                           </form>
        <form method="GET" action="{{ action('CountryController@create')}}"> 
                            <button class="btn btn-default pull-right">INSERTING</button> 
                            </form> 
        <div class="clearfix"></div>
        </div>
                  
    <div class="panel-body">
    <form class="form-horizontal">
        <div class="form-group">
            <label for="code" class="col-sm-4 control-label">Code:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="code" placeholder={{$country->code}} readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="latitude" class="col-sm-4 control-label">Latitude:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="latitude" placeholder={{$country->latitude}} readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="longitude" class="col-sm-4 control-label">Longitude:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="longitude" placeholder={{$country->longitude}} readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="name" class="col-sm-4 control-label">Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="name" placeholder={{$country->name}} readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="shippingCostMultiplier" class="col-sm-4 control-label">ShippingCostMultiplier:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" placeholder={{$country->shippingCostMultiplier}} readonly>
            </div>
        </div>
        </div> <!--panel-body-->
        </div>
    </form>
</div>
    <!--Table on Right-->
        <div class="col-md-4">
            </br>
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Select</th>
                    <th>Code</th>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($countries as $key => $value)
                    <tr>
                        <td align="center" ><a href="/country/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a></td>
                        <td>{{ $value->code }}</td>
                        <td>{{ $value->name }}</td>
                    </tr>

                    @endforeach
            </table>
        </div>
    </div>
</div>
@stop


