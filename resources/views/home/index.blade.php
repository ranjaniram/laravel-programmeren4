<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>MIKMAK Home Page</title>
    
    <link href="{{ secure_asset('css/style.css') }}" rel="stylesheet"> 
    <style type="text/css">
           *{
          box-sizing: border-box;
          -moz-box-sizing: border-box; 
          margin:0;
          padding:0;
          border:0;
        }     
     
      html,body{
            height: 100%;
            width :100%;
            position: relative;
            }
     
        .tower{
            height: calc(100% - 70px); 
            width:100% ; 
            background-color:white;
        }
        
        
        .floor{
            height: 100%;
            width :100%;
            background-color:#DAA520;
        }
        
        .control-panel{
            height: 13%;
            width :100%;
            background-color: black;
            text-align: right;
            color: white;
            padding: 2px 20px;
            font-size: 16px;
        }
        .show-room{
            height: 89%;
            width :90%;
            background-color:white;
            margin-left:5%;
            border-right:solid white 0.3em;
            border-bottom:solid white 0.1em;
        }
        
        .footer {
            height: 13%;
            width :100%;
            background-color: grey;
            color: white;
            padding: 10px;
            text-align: center;
            font-size: 20px;
            position: fixed; 
            
}
        .button {
            background-color: black;
            border: none;
            color: white;
            padding: 2px 20px 2px 15px;
            float: left;
            text-decoration: none;
            display: inline-block;
            font-size: 100%;
            margin: 4px 2px;
            text-align: center;
            cursor: pointer;
        }
        
        .button:hover {
        background-color: #DAA520;
        display: block;
        }
        
        h1 {
    margin: .67em 0;
    font-size: 2em
}
            
    </style>
</head>

<body>
    <main class="tower">
        <section class="floor">
            <nav class="control-panel">
                <a href=""></a>
                <h1>MIKMAK</h1>
            </nav>
            <article class="show-room">
            </article>
        </section>
    </main>
<div class="footer">
    <a href="https://p4rr-cloned-ranjaniramanuja.c9users.io/admin" class="button">Admin</a>
  <p>Footer</p>
</div>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
</body>

</html>