<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('Description',1024)->nullable(); 
            $table->string('Name', 255)->unique();
            $table->float('Price')->nullable();
            $table->float('ShippingCost')->nullable();
            $table->integer('TotalRating')->nullable();
            $table->string('Thumbnail', 255)->nullable();
            $table->string('Image', 255)->nullable();
            $table->float('DiscountPercentage')->nullable();
            $table->integer('Votes')->nullable();
            $table->integer('IdCategory')->unsigned();
            $table->foreign('IdCategory')->references('id')->on('categories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
