<!-- resources/views/customer/index.blade.php -->

@extends('layouts/template')

@section('content')
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<div class="container">
    <div class="row">
        <div class="col-md-8 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <h4 class="pull-left">
                    Customer
                    </h4>
                    <a href="{{url('/customer/create')}}" class="btn btn-default pull-right">Create Customer</a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

<!--Table on Right-->
    <div class="col-md-4"></br>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>select</th>
            <th>FirstName</th>
            <th>LastName</th>
        </tr>
    </thead>
    <tbody>
    @foreach($customers as $key => $value)
        <tr>
            <td align="center" ><a href="/customer/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a></td>
            <td>{{ $value->firstName }}</td>
            <td>{{ $value->lastname }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
    </div>
</div>
@endsection