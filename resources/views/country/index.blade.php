<!-- resources/views/country/index.blade.php -->

@extends('layouts/template')

@section('content')

@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<div class="container">
    <div class="row">
        <div class="col-md-8 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <h4 class="pull-left">
                    Country
                    </h4>
                    <a href="{{url('/country/create')}}" class="btn btn-default pull-right">Create Country</a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

<!--Table on Right-->
    <div class="col-md-4"></br>
<table class="table table-striped table-bordered ">
    <thead>
        <tr>
            <th>Select</th>
            <th>Code</th>
            <th>Name</th>
        </tr>
    </thead>
    <tbody>
    @foreach($countries as $key => $value)
        <tr>
            <td align="center" ><a href="/country/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a></td>
            <td>{{ $value->code }}</td>
            <td>{{ $value->name }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
</div>
    </div>
</div>
@endsection