<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Product;
use App\Category;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('product.index',compact('products'));
        //return view('product.index', array('products'=> $products));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::all();
        $categories = Category::all();
        return view('product.create',array('products' => $products, 'categories' => $categories));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //use Validator class  
        $validator = Validator::make($request->all(), [  
            'Description' => 'max:1024',  
            'Name' => 'required|unique:products|max:255',
            'Price' => 'regex:/^\\d+\.?\\d+?$/',   
            'ShippingCost' => 'regex:/^[+-]?\\d+\.?\\d+?$/',   
            'TotalRating' => 'max:10|regex:/^[0-9]+?$/',  
            'Thumbnail' => 'max:255',  
            'Image' => 'max:255',  
            'DiscountPercentage' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',  
            'Votes' => 'max:11|regex:/^[0-9]+?$/',  
            'IdCategory' => 'required|max:10',   
        ]);  
         
         if ($validator->fails()) {  
            return redirect('product/create')  
                        ->withErrors($validator)  
                        ->withInput($request->all());  
        }  
        
         //store
        $product =new Product;
        $product->Description = empty($request->input('Description')) ? null : $request->input('Description');
        $product->Name = $request->input('Name');
        $product->Price = empty($request->input('Price'))? null : $request->input('Price');
        $product->ShippingCost = empty($request->input('ShippingCost')) ? null : $request->input('ShippingCost');
        $product->TotalRating = empty($request->input('TotalRating')) ? null : $request->input('TotalRating');
        $product->Thumbnail = empty($request->input('Thumbnail')) ? null : $request->input('Thumbnail');
        $product->Image = empty($request->input('Image')) ? null : $request->input('Image');
        $product->DiscountPercentage = empty($request->input('DiscountPercentage')) ? null : $request->input('DiscountPercentage');
        $product->Votes = empty($request->input('Votes')) ? null : $request->input('Votes');
        $product->IdCategory = $request->input('IdCategory');
        $product->save();
        
        // redirect  
        Session::flash('message', 'Successfully created Product!');  
        return redirect('/product'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        $category=Category::find($id);
        $products = Product::all();
        $categories = Category::all();
        
        return view('product.show',compact('product','products','categories','category'));
        //return view('product.show',array('products' => $products,'product' => $product,  'category' => $category,'categories' => $categories));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $products = Product::all();
        $product = Product::find($id);
        $categories = Category::all();
        $category=Category::find($id);
        return view('product.edit',compact('product','products','categories','category'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [  
            'Description' => 'max:1024',  
            'Name' => 'required|max:255',
            'Price' => 'regex:/^\\d+\.?\\d+?$/',   
            'ShippingCost' => 'regex:/^[+-]?\\d+\.?\\d+?$/',   
            'TotalRating' => 'max:10|regex:/^[0-9]+?$/',  
            'Thumbnail' => 'max:255',  
            'Image' => 'max:255',  
            'DiscountPercentage' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',  
            'Votes' => 'max:11|regex:/^[0-9]+?$/',  
            'IdCategory' => 'required|max:10',   
        ]);  
         
         if ($validator->fails()) {  
            return redirect('product/'.$id.'/edit')  
                        ->withErrors($validator)  
                        ->withInput($request->all());  
        } 
        
        //update
        $product = Product::find($id);
        $product->Description = empty($request->input('Description')) ? null : $request->input('Description');
        $product->Name = $request->input('Name');
        $product->Price = empty($request->input('Price'))? null : $request->input('Price');
        $product->ShippingCost = empty($request->input('ShippingCost')) ? null : $request->input('ShippingCost');
        $product->TotalRating = empty($request->input('TotalRating')) ? null : $request->input('TotalRating');
        $product->Thumbnail = empty($request->input('Thumbnail')) ? null : $request->input('Thumbnail');
        $product->Image = empty($request->input('Image')) ? null : $request->input('Image');
        $product->DiscountPercentage = empty($request->input('DiscountPercentage')) ? null : $request->input('DiscountPercentage');
        $product->Votes = empty($request->input('Votes')) ? null : $request->input('Votes');
        $product->IdCategory = $request->input('IdCategory');
        $product->save();
        
        // redirect  
        Session::flash('message', 'Successfully updated Product!');  
        return redirect('/product'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        return redirect('/product');
    }
}