<!-- resources/views/product/show.blade.php -->

@extends('layouts/template')

@section('content')
<div class="container">
    <div class="row"> 
     <div class="col-md-8"> 
     <div class="panel panel-default">
        <div class="panel-heading">
        <h4 class="pull-left">
        Product : {{ $product->Name }}
        </h4>
        <a href="{{ url('product')}}" class="btn btn-default pull-right" >CANCEL</a>
        <form method="POST" action="{{ action('ProductController@destroy', $product->id) }}"> 
                            <input type="hidden" name="_method" value="DELETE"/>     
                            {{ csrf_field() }} 
                             <button class="btn btn-default pull-right">DELETE</button> 
                             </form>
        <form method="GET" action="{{ action('ProductController@edit', $product->id) }}"> 
                            <button class="btn btn-default pull-right">UPDATING</button> 
                           </form>
        <form method="GET" action="{{ action('ProductController@create')}}"> 
                            <button class="btn btn-default pull-right">INSERTING</button> 
                            </form> 
        <div class="clearfix"></div>
        </div>
                  
    <div class="panel-body">
    <form class="form-horizontal">
        
        <div class="form-group">
            <label for="Name" class="col-sm-2 control-label">Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="Name"  value="<?php echo $product->Name;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="Description" class="col-sm-2 control-label">Description:</label>
            <div class="col-sm-6">
                <textarea class="form-control" name="Description"  readonly>{{ $product->Description }}</textarea>
            </div>
        </div>
        
        <div class="form-group">
            <label for="Price" class="col-sm-2 control-label">Price:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="Price"  value="<?php echo $product->Price;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="ShippingCost" class="col-sm-2 control-label">ShippingCost:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="ShippingCost"  value="<?php echo $product->ShippingCost;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="TotalRating" class="col-sm-2 control-label">TotalRating:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="TotalRating"  value="<?php echo $product->TotalRating;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="Thumbnail" class="col-sm-2 control-label">Thumbnail:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="Thumbnail"  value="<?php echo $product->Thumbnail;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="Image" class="col-sm-2 control-label">Image:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="Image"  value="<?php echo $product->Image;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="DiscountPercentage" class="col-sm-2 control-label">DiscountPercentage:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="DiscountPercentage"  value="<?php echo $product->DiscountPercentage;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="Votes" class="col-sm-2 control-label">Votes:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="Votes"  value="<?php echo $product->Votes;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="IdCategory" class="col-sm-2 control-label">IdCategory:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="IdCategory"  value="<?php echo $category->Name;?>" readonly>
            </div>
        </div>
         </div> <!--panel-body-->
        </div>
    </form>
</div>
 <!--Table on Right-->
        <div class="col-md-4">
            </br>
            <table class="table table-striped table-bordered">
                <tr>
                    <th>select</th>
                    <th>ID</th>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                     @foreach($products as $key => $value)
                    <tr>
                        <td align="center" ><a href="/product/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a></td>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->Name }}</td>
                    </tr>

                    @endforeach
            </table>
        </div>
    </div>
@stop

