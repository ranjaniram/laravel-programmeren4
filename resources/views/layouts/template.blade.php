<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>MIKMAK Admin Page</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="{{ secure_asset('css/style2.css') }}" rel="stylesheet"> 
</head>
<body>
    <main class="tower2">
         <nav class="control-panel">
                <div style="font-size:30px;cursor:pointer;float:left" onclick="openNav()">&#9776; </div>
                <div id="mySidenav" class="sidenav">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                <a href="{{ URL::to('http://p4rr-cloned-ranjaniramanuja.c9users.io/') }}">Home</a>
                <a href="{{ URL::to('http://p4rr-cloned-ranjaniramanuja.c9users.io/admin') }}">Admin</a>
                <a href="{{ URL::to('country') }}">Country</a>
                <a href="{{ URL::to('customer') }}">Customer</a>
                <a href="{{ URL::to('category') }}">Category</a>
                <a href="{{ URL::to('product') }}">Product</a>
                </div>
                <h1><a href="{{ URL::to('https://p4rr-cloned-ranjaniramanuja.c9users.io/admin') }}"></a>MIKMAK</h1>
            </nav>
        <section class="floor">
            <article class="show-room">
                <!--<!(div[class="tile"]>(a[class="mask"]>h2+p)+h1+p)*6-->
                
                @yield('content')
                
            </article>
        </section>
    </main>
<div class="footer">
  <p>Footer</p>
</div>

<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}
</script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>

</html>
   

