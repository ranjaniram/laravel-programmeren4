<!-- resources/views/product/edit.blade.php -->

@extends('layouts/template') @section('content') @if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="container">
    <div class="row">
        <div class="col-md-8 ">

            <form method="post" action="{{ action('ProductController@update',$product->id) }}">
                {{ csrf_field() }}
                <!--  hidden  _method field to spoof these HTTP verbs PUT, PATCH, or DELETE-->
                {{ method_field('PUT') }}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="pull-left">
                            Update : {{ $product->Name }}
                        </h4>
                        <button class="pull-right"><a href="{{ url('product')}}" class="btn btn-default" >Cancel</a></button>
                        <button class="pull-right"> {{ Form::submit('Update', array('class' => 'btn btn-default')) }} </button>
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label for="Name">Name:</label>
                            <div class="col-6">
                            <input type="text" name="Name" value="<?php echo $product->Name;?>" class="form-control" />
                        </div></div>
                        
                        <div class="form-group">
                            <label for="Description">Description:</label><br/>
                             <div class="col-6">
                            <textarea class="form-control" name="Description" >{{ $product->Description }}</textarea>
                        </div></div>
                        
                        <div class="form-group">
                            <label for="Price">Price:</label>
                            <div class="col-6">
                            <input type="text" name="Price" value="<?php echo $product->Price;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="ShippingCost">ShippingCost:</label>
                             <div class="col-6">
                            <input type="text" name="ShippingCost" value="<?php echo $product->ShippingCost;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="TotalRating">TotalRating:</label>
                            <div class="col-6">
                            <input type="text" name="TotalRating" value="<?php echo $product->TotalRating;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="Thumbnail">Thumbnail:</label>
                            <div class="col-6">
                            <input type="text" name="Thumbnail" value="<?php echo $product->Thumbnail;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="Image">Image:</label>
                            <div class="col-6">
                            <input type="text" name="Image" value="<?php echo $product->Image;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            {!! Form::Label('IdCategory', 'IdCategory:') !!}
                            <div class="col-6">
                            <select class="form-control form-control-lg" name="IdCategory"> 
                                @foreach($categories as $Category) 
                                 <option @if($product->IdCategory == $Category->id)selected="selected"@endif  
                                 <option value="{{$Category->id}}"><?php echo $Category->Name;?></option>
                                @endforeach 
                            </select></div>
                        </div>

                        <div class="form-group">
                            <label for="DiscountPercentage">DiscountPercentage:</label>
                            <div class="col-6">
                            <input type="text" name="DiscountPercentage" value="<?php echo $product->DiscountPercentage;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="Votes">Votes:</label>
                            <div class="col-6">
                            <input type="text" name="Votes" value="<?php echo $product->Votes;?>" class="form-control" />
                        </div></div>
                    </div> <!--panel-body-->
                </div>
            </form>
        </div>

            <!--Table on Right-->
        <div class="col-md-4">
            </br>
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Select</th>
                    <th>ID</th>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                     @foreach($products as $key => $value)
                    <tr>
                        <td align="center" ><a href="/product/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a></td>
                        <td>{{ $value->id }}</td>
                        <td>{{ $value->Name }}</td>
                    </tr>

                    @endforeach
            </table>
        </div>
    </div>
    </div>
@stop