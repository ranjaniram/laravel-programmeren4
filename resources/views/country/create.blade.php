<!-- resources/views/country/create.blade.php -->

@extends('layouts/template') @section('content') @if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="container">
    <div class="row">
        <div class="col-md-8 ">

            <form method="post" action="{{action('CountryController@store')}}">
                {{ csrf_field() }}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="pull-left">
                            Create a Country
                        </h4>
                        <button class="pull-right"><a href="{{ url('country')}}" class="btn btn-default" >Cancel</a></button>
                        <button class="pull-right">{!! Form::submit('Create', array('class' => 'btn btn-default')) !!}</button>
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label for="code">Code:</label>
                            <div class="col-6">
                                <input type="text" name="code" value="{{ old('code') }}" class="form-control" /> @if ($errors->has('code'))
                                <p class="form-text text-muted">{{ $errors->first('code') }}</p> @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="latitude">Latitude:</label>
                            <div class="col-6">
                                <input type="text" name="latitude" value="{{ old('latitude') }}" class="form-control" /> @if ($errors->has('latitude'))
                                <p class="form-text text-muted">{{ $errors->first('latitude') }}</p> @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="longitude">Longitude:</label>
                            <div class="col-6">
                                <input type="text" name="longitude" value="{{ old('longitude') }}" class="form-control" /> @if ($errors->has('longitude'))
                                <p class="form-text text-muted">{{ $errors->first('longitude') }}</p> @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name">Name:</label>
                            <div class="col-6">
                                <input type="text" name="name" value="{{ old('name') }}" class="form-control" /> @if ($errors->has('name'))
                                <p class="form-text text-muted">{{ $errors->first('name') }}</p> @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="shippingCostMultiplier">ShippingCostMultiplier:</label>
                            <div class="col-6">
                                <input type="text" name="shippingCostMultiplier" value="{{ old('shippingCostMultiplier') }}" class="form-control" /> @if ($errors->has('shippingCostMultiplier'))
                                <p class="form-text text-muted">{{ $errors->first('shippingCostMultiplier') }}</p> @endif
                            </div>
                        </div>
                    </div>
                    <!--panel-body-->
                </div>
            </form>
        </div>

        <!--Table on Right-->
        <div class="col-md-4">
            </br>
            </br>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Select</th>
                        <th>Code</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($countries as $key => $value)
                    <tr>
                        <td align="center">
                            <a href="/country/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a>
                        </td>
                        <td>{{ $value->code }}</td>
                        <td>{{ $value->name }}</td>
                    </tr>
                    @endforeach
            </table>
        </div>
    </div>
</div>


@endSection