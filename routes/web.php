<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Car Controller
Route::resource('cars', 'CarController');


//Country Controller
Route::resource('country', 'CountryController'); 

//Customer Controller
Route::resource('customer', 'CustomerController'); 

//Product Controller
Route::resource('product', 'ProductController'); 

//Category Controller
Route::resource('category', 'CategoryController'); 

/* Home */
Route::get('/', 'HomeController@index');

Route::get('admin', 'HomeController@admin');