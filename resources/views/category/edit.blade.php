<!-- resources/views/category/edit.blade.php -->

@extends('layouts/template') @section('content') @if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="container">
    <div class="row">
        <div class="col-md-8 ">
            <form method="post" action="{{ action('CategoryController@update',$category->id) }}">
                {{ csrf_field() }}
                <!--  hidden  _method field to spoof these HTTP verbs PUT, PATCH, or DELETE-->
                {{ method_field('PUT') }}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="pull-left">
                            Update {{ $category->Name }}
                        </h4>
                        <button class="pull-right"><a href="{{ url('category')}}" class="btn btn-default" >Cancel</a></button>
                        <button class="pull-right"> {{ Form::submit('Update', array('class' => 'btn btn-default')) }} </button>
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label for="Name">Name:</label>
                            <div class="col-6">
                            <input type="text" name="Name" value="<?php echo $category->Name; ?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="Description">Description:</label>
                            <div class="col-6">
                                <textarea class="form-control" name="Description" >{{ $category->Description }}</textarea>
                        </div></div>
                    </div>
                    <!--panel-body-->
                </div>
            </form>
        </div>

        <!--Table on Right-->
        <div class="col-md-4">
            </br>
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Select</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($categories as $key => $value)
                    <tr>
                         <td align="center" ><a href="/category/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a></td>
                        <td>{{ $value->Name }}</td>
                        <td>{{ $value->Description }}</td>
                    </tr>
                    @endforeach
            </table>
        </div>
    </div>
</div>
@endSection
