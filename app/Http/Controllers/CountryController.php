<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Routes;
use App\Country;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries =Country::all();
        //return view('country.index',compact('countries'));
        return view('country.index', array('countries'=> $countries));
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
         $countries =Country::all();
         return view('country.create', array('countries'=> $countries));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //use Validator class 
           $validator = Validator::make($request->all(), [ 
            'code' => 'required|unique:countries|max:2', 
            'name' => 'required|unique:countries|max:255', 
            'latitude' => 'regex:/^[+-]?\\d+\.?\\d+?$/', 
            'longitude' => 'regex:/^[+-]?\\d+\.?\\d+?$/',  
            'shippingCostMultiplier' => 'regex:/^[+-]?\\d+\.?\\d+?$/', 
        ]); 
        
       if ($validator->fails()) { 
             return redirect('country/create') 
                     ->withErrors($validator) 
                     ->withInput(); 
        } 
         
        $country = new Country; 
        $country->Code = $request->input('code'); 
        $country->Name = $request->input('name'); 
        $country->Latitude = empty($request->input('latitude')) ? null : $request->input('latitude'); 
        $country->Longitude = empty($request->input('longitude')) ? null : $request->input('longitude'); 
        $country->ShippingCostMultiplier = empty($request->input('shippingCostMultiplier')) ? null : $request->input('shippingCostMultiplier'); 
        $country->save(); 
         
         // redirect 
        Session::flash('message', 'Successfully created country!'); 
        return redirect('/country');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $country = Country::find($id);
         $countries =Country::all();
        return view('country.show',compact('country','countries'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::find($id);
        $countries =Country::all();
        return view('country.edit',compact('country','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         //use Validator class 
        $validator = Validator::make($request->all(), [ 
            'code' => 'required|unique:countries,code,'.$id.'|max:2', 
            'name' => 'required|unique:countries,name,'.$id.',id|max:255', 
            'latitude' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', 
            'longitude' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', 
            'shippingCostMultiplier' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/', 
        ]); 
        
         if ($validator->fails()) { 
            return redirect('country/'.$id.'/edit') 
                        ->withErrors($validator) 
                        ->withInput(); 
        } 
        
        
        //Update
        $country = Country::find($id);
        $country->Code = $request->input('code'); 
        $country->Name = $request->input('name'); 
        $country->Latitude = empty($request->input('latitude')) ? null : $request->input('latitude'); 
        $country->Longitude = empty($request->input('longitude')) ? null : $request->input('longitude'); 
        $country->ShippingCostMultiplier = empty($request->input('shippingCostMultiplier')) ? null : $request->input('shippingCostMultiplier'); 
        $country->save(); 
         
        
        // redirect
        Session::flash('message', 'Successfully updated country!');
        return redirect('/country'); 
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Country::find($id)->delete();
        return redirect('/country');
        //return Redirect::route('country.delete')->with('success', 'Country is deleted!');
    }
}
