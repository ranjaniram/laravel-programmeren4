<!-- resources/views/country/edit.blade.php -->

@extends('layouts/template') @section('content') @if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="container">
    <div class="row">
        <div class="col-md-8 ">
            <form method="post" action="{{ action('CountryController@update',$country->id) }}">
                {{ csrf_field() }}
                <!--  hidden  _method field to spoof these HTTP verbs PUT, PATCH, or DELETE-->
                {{ method_field('PUT') }}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="pull-left">
                            Update {{ $country->name }}
                        </h4>
                        <button class="pull-right"><a href="{{ url('country')}}" class="btn btn-default" >Cancel</a></button>
                        <button class="pull-right"> {{ Form::submit('Update', array('class' => 'btn btn-default')) }} </button>
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group col-md-10">
                            <label for="code">Code:</label>
                            <div class="col-6">
                                <input type="text" name="code" value="<?php echo $country->code; ?>" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group col-md-10">
                            <label for="latitude">Latitude:</label>
                            <div class="col-6">
                                <input type="text" name="latitude" value="<?php echo $country->latitude; ?> " class="form-control" />
                            </div>
                        </div>

                        <div class="form-group col-md-10">
                            <label for="longitude">Longitude:</label>
                            <div class="col-6">
                                <input type="text" name="longitude" value="<?php echo $country->longitude; ?>" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group col-md-10">
                            <label for="name">Name:</label>
                            <div class="col-6">
                                <input type="text" name="name" value="<?php echo $country->name; ?>" class="form-control" />
                            </div>
                        </div>

                        <div class="form-group col-md-10">
                            <label for="shippingCostMultiplier">ShippingCostMultiplier:</label>
                            <div class="col-6">
                                <input type="text" name="shippingCostMultiplier" value="<?php echo $country->shippingCostMultiplier;?>" class="form-control" />
                            </div>
                        </div>
                    </div>
                    <!--panel-body-->
                </div>
            </form>
        </div>
        <!--Table on Right-->
        <div class="col-md-4">
            </br>
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Select</th>
                    <th>Code</th>
                    <th>Name</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($countries as $key => $value)
                    <tr>
                        <td align="center">
                            <a href="/country/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a>
                        </td>
                        <td>{{ $value->code }}</td>
                        <td>{{ $value->name }}</td>
                    </tr>

                    @endforeach
            </table>
        </div>
    </div>
</div>


@endSection