<!-- resources/views/category/show.blade.php -->

@extends('layouts/template')

@section('content')

 <div class="container">
    <div class="row"> 
     <div class="col-md-8"> 
            <div class="panel panel-default">
        <div class="panel-heading">
       <h4 class="pull-left">Category : {{ $category->Name }}</h4> 
        <a href="{{ url('category')}}" class="btn btn-default pull-right" >CANCEL</a>
        <form method="POST" action="{{ action('CategoryController@destroy', $category->id) }}"> 
                            <input type="hidden" name="_method" value="DELETE"/>     
                            {{ csrf_field() }} 
                             <button class="btn btn-default pull-right">DELETE</button> 
                             </form>
        <form method="GET" action="{{ action('CategoryController@edit', $category->id) }}"> 
                            <button class="btn btn-default pull-right">UPDATING</button> 
                           </form>
        <form method="GET" action="{{ action('CategoryController@create')}}"> 
                            <button class="btn btn-default pull-right">INSERTING</button> 
                            </form> 
        <div class="clearfix"></div>
        </div>
                  
    <div class="panel-body">
    <form class="form-horizontal">
        <div class="form-group">
            <label for="Name" class="col-sm-2 control-label">Name:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="Name" placeholder={{$category->Name}} readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="Description" class="col-sm-2 control-label">Description:</label>
            <div class="col-sm-6">
                <textarea class="form-control" name="Description" readonly>{{ $category->Description }}</textarea>
            </div>
        </div>
    </div> <!--panel-body-->
        </div>
    </form>
</div>
<!--Table on Right-->
        <div class="col-md-4">
            </br>
            <table class="table table-striped table-bordered">
                <tr>
                    <th>Select</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                   @foreach($categories as $key => $value)
                    <tr>
                         <td align="center" ><a href="/category/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a></td>
                        <td>{{ $value->Name }}</td>
                        <td>{{ $value->Description }}</td>
                    </tr>
                    @endforeach
            </table>
        </div>
    </div>
</div>
@endSection





