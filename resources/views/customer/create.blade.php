<!-- resources/views/customer/create.blade.php -->

@extends('layouts/template') 
 
@section('content') 

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif  

<div class="container">
    <div class="row">
        <div class="col-md-8 ">
            <form method="post" action="{{action('CustomerController@store')}}"> 
                {{ csrf_field() }} 
                <div class="panel panel-default">
                    <div class="panel-heading">
                     <h4 class="pull-left">
                           Create a Customer
                    </h4>
                    <button class="pull-right"><a href="{{ url('customer')}}" class="btn btn-default" >Cancel</a></button>
                    <button class="pull-right">{!! Form::submit('Create', array('class' => 'btn btn-default')) !!}</button>
                    <div class="clearfix"></div>
                    </div>
                  
            <div class="panel-body">
         
                <div class="form-group"> 
                <label for="nickname">Nickname:</label>
                <div class="col-6">
                <input type="text" name="nickname" value="{{ old('nickname') }}" class="form-control"/> 
                </div></div> 
                
                <div class="form-group"> 
                <label for="firstName">FirstName:</label>
                <div class="col-6">
                <input type="text" name="firstName"  value="{{ old('firstName') }}" class="form-control"/> 
                </div></div> 
                 
                <div class="form-group"> 
                <label for="lastname">Lastname:</label>
                <div class="col-6">
                <input type="text" name="lastname"  value="{{ old('lastname') }}" class="form-control"/> 
                </div></div> 
                 
                <div class="form-group"> 
                <label for="address1">Address1:</label>
                <div class="col-6">
                <input type="text" name="address1" value="{{ old('address1') }}" class="form-control"/> 
                </div></div> 
                
                <div class="form-group"> 
                <label for="address1">Address2:</label>
                <div class="col-6">
                <input type="text" name="address2" value="{{ old('address2') }}" class="form-control"/> 
                </div></div> 
                
                <div class="form-group"> 
                <label for="city">City:</label>
                <div class="col-6">
                <input type="text" name="city" value="{{ old('city') }}" class="form-control"/> 
                </div></div> 
                
                <div class="form-group"> 
                <label for="region">Region:</label>
                <div class="col-6">
                <input type="text" name="region" value="{{ old('region') }}" class="form-control"/> 
                </div></div> 
                
                <div class="form-group"> 
                <label for="postalCode">PostalCode:</label>
                <div class="col-6">
                <input type="text" name="postalCode"  value="{{ old('postalCode') }}" class="form-control"/> 
                </div></div> 
                
                <div class="form-group"> 
                <!--<label for="idcountry">IdCountry:</label><br/> -->
                {!! Form::Label('idcountry', 'IdCountry:') !!}
                <div class="col-6">
                <select class="form-control form-control-lg" name="idcountry" > 
                    @foreach($countries as $country) 
                     <option value="{{$country->id}}">{{$country->name}}</option>
                    @endforeach 
                </select></div>
                </div>
                
                <div class="form-group"> 
                <label for="phone">Phone:</label>
                <div class="col-6">
                <input type="text" name="phone" value="{{ old('phone') }}" class="form-control"/> 
                </div></div>
                 
                <div class="form-group"> 
                <label for="mobile">Mobile:</label>
                <div class="col-6">
                <input type="text" name="mobile" value="{{ old('mobile') }}" class="form-control"/> 
                </div></div>
            </div> <!--panel-body-->
        </div>
    </form>
</div>
 
 <!--Table on Right-->
        <div class="col-md-4">
            </br>
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>select</th>
                        <th>FirstName</th>
                        <th>LastName</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($customers as $key => $value)
                    <tr>
                        <td align="center" ><a href="/customer/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a></td>
                        <td>{{ $value->firstName }}</td>
                        <td>{{ $value->lastname }}</td>
                    </tr>
                    @endforeach
            </table>
        </div>
    </div>
</div>
@endSection