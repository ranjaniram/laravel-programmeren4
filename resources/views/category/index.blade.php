<!-- resources/views/category/index.blade.php -->

@extends('layouts/template')

@section('content')

@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<div class="container">
    <div class="row">
        <div class="col-md-8 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <h4 class="pull-left">
                    Category 
                    </h4>
                    <a href="{{url('/category/create')}}" class="btn btn-default pull-right">Create Category</a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

<!--Table on Right-->
    <div class="col-md-4"></br>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Select</th>
                    <th>Name</th>
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
            @foreach($categories as $key => $value)
                <tr>
                    <td align="center" ><a href="/category/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a></td>
                    <td>{{ $value->Name }}</td>
                    <td>{{ $value->Description }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    </div>
</div>
@endsection