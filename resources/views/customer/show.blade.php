<!-- resources/views/customer/show.blade.php -->

@extends('layouts/template')

@section('content')

  <div class="container">
    <div class="row"> 
     <div class="col-md-8"> 
      <div class="panel panel-default">
        <div class="panel-heading">
        <h4 class="pull-left">Customer : {{ $customer->firstName}}</h4>
        <a href="{{ url('customer')}}" class="btn btn-default pull-right" >CANCEL</a>
        <form method="POST" action="{{ action('CustomerController@destroy', $customer->id) }}"> 
                            <input type="hidden" name="_method" value="DELETE"/>     
                            {{ csrf_field() }} 
                             <button class="btn btn-default pull-right">DELETE</button> 
                             </form>
        <form method="GET" action="{{ action('CustomerController@edit', $customer->id) }}"> 
                            <button class="btn btn-default pull-right">UPDATING</button> 
                           </form>
        <form method="GET" action="{{ action('CustomerController@create')}}"> 
                            <button class="btn btn-default pull-right">INSERTING</button> 
                            </form> 
        <div class="clearfix"></div>
        </div>
                  
    <div class="panel-body">
    <form class="form-horizontal">
        <div class="form-group">
            <label for="nickname" class="col-sm-2 control-label">NickName:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="nickname" value="<?php echo $customer->nickname;?>" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="firstName" class="col-sm-2 control-label">FirstName:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="firstName" value="<?php echo $customer->firstName;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="lastname" class="col-sm-2 control-label">LastName:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="lastname" value="<?php echo $customer->lastname;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="address1" class="col-sm-2 control-label">Address1:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="address1" value="<?php echo $customer->address1;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="address2" class="col-sm-2 control-label">Address2:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="address2" value="<?php echo $customer->address2;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="city" class="col-sm-2 control-label">City:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="city" value="<?php echo $customer->city;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="region" class="col-sm-2 control-label">Region:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="region" value="<?php echo $customer->region;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="postalCode" class="col-sm-2 control-label">PostalCode:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="postalCode"  value="<?php echo $customer->postalCode;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="idcountry" class="col-sm-2 control-label">IdCountry:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="idcountry"  value="<?php echo $country->name;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="phone" class="col-sm-2 control-label">Phone:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="phone" value="<?php echo $customer->phone;?>" readonly>
            </div>
        </div>
        
        <div class="form-group">
            <label for="mobile" class="col-sm-2 control-label">Mobile:</label>
            <div class="col-sm-6">
                <input type="text" class="form-control" id="mobile" value="<?php echo $customer->mobile;?>" readonly>
            </div>
        </div>
    </div> <!--panel-body-->
        </div>
    </form>
</div>
 <!--Table on Right-->
        <div class="col-md-4">
            </br>
           <table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>select</th>
            <th>FirstName</th>
            <th>LastName</th>
        </tr>
    </thead>
    <tbody>
    @foreach($customers as $key => $value)
        <tr>
            <td align="center" ><a href="/customer/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a></td>
            <td>{{ $value->firstName }}</td>
            <td>{{ $value->lastname }}</td>
        </tr>
    @endforeach
            </table>
        </div>
    </div>
</div>
@endSection