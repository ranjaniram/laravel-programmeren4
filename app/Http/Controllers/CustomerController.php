<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use App\Customer;
use App\Country;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customers =Customer::all();
        return view('customer.index',compact('customers'));
       //return view('customer.index', array('customers'=> $customers));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        $customers = Customer::all();
        return view('customer.create',array('customers' => $customers, 'countries' => $countries)); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         //use Validator class 
        $validator = Validator::make($request->all(), [ 
            'nickname' => 'required|max:10', 
            'firstName' => 'required|max:255|regex:/^[A-Z][a-z]*$/', 
            'lastname' => 'required|max:255|regex:/^[A-Z][a-z]*$/', 
            'address1' => 'required|max:255', 
            'address2' => 'max:255', 
            'city' => 'required|max:255', 
            'region' => 'max:80', 
            'postalCode' => 'max:20|regex:/^\d{4}$/', 
            'phone' => 'max:40|regex:/^\d{10}$/', 
            'mobile' => 'max:40|regex:/^\d{10}$/',  
            'idcountry' => 'required|max:10', 
        ]); 
        
         if ($validator->fails()) { 
            return redirect('customer/create') 
                        ->withErrors($validator) 
                        ->withInput($request->all()); 
        } 
        
        //store
        $customer =new Customer;
        $customer->nickname = $request->input('nickname');
        $customer->firstName = $request->input('firstName');
        $customer->lastname = $request->input('lastname');
        $customer->address1 = $request->input('address1');
        $customer->address2 =empty($request->input('address2')) ? null : $request->input('address2');
        $customer->city = $request->input('city');
        $customer->region = empty($request->input('region')) ? null : $request->input('region');
        $customer->postalCode = $request->input('postalCode');
        $customer->idcountry = $request->input('idcountry');
        $customer->phone = empty($request->input('phone')) ? null : $request->input('phone');
        $customer->mobile =  empty($request->input('mobile')) ? null : $request->input('mobile');
        $customer->save();
        
        Session::flash('message', 'Successfully created customer!'); 
        return redirect('/customer'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customers = Customer::all(); 
        $customer = Customer::find($id);
        $countries = Country::all(); 
        $country = Country::find($customer->idcountry); 
        
        return view('customer.show',compact('customers','customer','countries','country'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customers = Customer::all(); 
        $customer = Customer::find($id);
        $countries = Country::all(); 
        $country = Country::find($customer->idcountry); 
        return view('customer.edit',compact('customers','customer','countries','country'));
        //return view('customer.edit', array('customers'=> $customers,'customer'=> $customer,'countries'=>$countries)); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //use Validator class
         $validator = Validator::make($request->all(), [ 
            'nickname' => 'required|max:10', 
            'firstName' => 'required|max:255|regex:/^[A-Z][a-z]*$/', 
            'lastname' => 'required|max:255|regex:/^[A-Z][a-z]*$/', 
            'address1' => 'required|max:255', 
            'address2' => 'max:255', 
            'city' => 'required|max:255', 
            'region' => 'max:80', 
            'postalCode' => 'max:20|regex:/^\d{4}$/', 
            'phone' => 'max:40', 
            'mobile' => 'max:40|regex:/^\d{10}$/',  
            'idcountry' => 'required|max:10', 
        ]); 
        
         if ($validator->fails()) { 
            return redirect('customer/'.$id.'/edit') 
                        ->withErrors($validator) 
                        ->withInput($request->all()); 
        } 
        $customer = Customer::find($id);
        $customer->nickname = $request->input('nickname');
        $customer->firstName = $request->input('firstName');
        $customer->lastname = $request->input('lastname');
        $customer->address1 = $request->input('address1');
        $customer->address2 =empty($request->input('address2')) ? null : $request->input('address2');
        $customer->city = $request->input('city');
        $customer->region = empty($request->input('region')) ? null : $request->input('region');
        $customer->postalCode = $request->input('postalCode');
        $customer->idcountry = $request->input('idcountry');
        $customer->phone = empty($request->input('phone')) ? null : $request->input('phone');
        $customer->mobile =  empty($request->input('mobile')) ? null : $request->input('mobile');
        $customer->save();
        
        Session::flash('message', 'Successfully updated customer!'); 
        return redirect('/customer'); 
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Customer::find($id)->delete();
        return redirect('/customer');
    }
}
