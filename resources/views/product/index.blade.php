<!-- resources/views/product/index.blade.php -->

@extends('layouts/template')

@section('content')
    
    <!-- will be used to show any messages -->
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

<div class="container">
    <div class="row">
        <div class="col-md-8 ">
            <div class="panel panel-default">
                <div class="panel-heading">
                     <h4 class="pull-left">
                    Product 
                    </h4>
                    <a href="{{url('/product/create')}}" class="btn btn-default pull-right">Create Product</a>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

<!--Table on Right-->
    <div class="col-md-4"></br>
        <table class="table table-striped table-bordered ">
            <thead>
                <tr>
                    <th>Select</th>
                    <th>ID</th>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
            @foreach($products as $key => $value)
                <tr>
                    <td align="center" ><a href="/product/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a></td>
                    <td>{{ $value->id }}</td>
                    <td>{{ $value->Name }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</div>
@endsection