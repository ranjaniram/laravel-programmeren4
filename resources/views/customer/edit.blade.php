<!-- resources/views/customer/edit.blade.php -->

@extends('layouts/template') @section('content') @if (count($errors) > 0)
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<div class="container">
    <div class="row">
        <div class="col-md-8 ">
            <form method="post" action="{{ action('CustomerController@update',$customer->id) }}">
                {{ csrf_field() }}
                <!--  hidden  _method field to spoof these HTTP verbs PUT, PATCH, or DELETE-->
                {{ method_field('PUT') }}
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="pull-left">
                            Update {{ $customer->firstName }}
                        </h4>
                        <button class="pull-right"><a href="{{ url('customer')}}" class="btn btn-default" >Cancel</a></button>
                        <button class="pull-right"> {{ Form::submit('Update', array('class' => 'btn btn-default')) }} </button>
                        <div class="clearfix"></div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label for="nickname">Nickname:</label>
                             <div class="col-6">
                            <input type="text" name="nickname" value="<?php echo $customer->nickname;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="firstName">FirstName:</label>
                             <div class="col-6">
                            <input type="text" name="firstName" value="<?php echo $customer->firstName;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="lastname">Lastname:</label>
                             <div class="col-6">
                            <input type="text" name="lastname" value="<?php echo $customer->lastname;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="address1">Address1:</label>
                            <div class="col-6">
                            <input type="text" name="address1" value="<?php echo $customer->address1;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="address2">Address2:</label>
                             <div class="col-6">
                            <input type="text" name="address2" value="<?php echo $customer->address2;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="city">City:</label>
                             <div class="col-6">
                            <input type="text" name="city" value="<?php echo $customer->city;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="region">Region:</label>
                             <div class="col-6">
                            <input type="text" name="region" value="<?php echo $customer->region;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="postalCode">PostalCode:</label>
                             <div class="col-6">
                            <input type="text" name="postalCode" value="<?php echo $customer->postalCode;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="idcountry">IdCountry:</label>
                             <div class="col-6">
                            <select id="idcountry" name="idcountry" class="form-control form-control-lg"> 
                                @foreach($countries as $country) 
                                <option @if($customer->idcountry == $country->id)selected="selected"@endif  
                                <option value="{{$country->id}}">{{$country->name}}</option> 
                                @endforeach 
                            </select></div>
                        </div>

                        <div class="form-group">
                            <label for="phone">Phone:</label>
                             <div class="col-6">
                            <input type="text" name="phone" value="<?php echo $customer->phone;?>" class="form-control" />
                        </div></div>

                        <div class="form-group">
                            <label for="mobile">Mobile:</label>
                             <div class="col-6">
                            <input type="text" name="mobile" value="<?php echo $customer->mobile;?>" class="form-control" />
                        </div></div>
                    </div>
                    <!--panel-body-->
                </div>
            </form>
        </div>

        <!--Table on Right-->
        <div class="col-md-4">
            </br>
            <table class="table table-striped table-bordered ">
                <thead>
                    <tr>
                        <th>select</th>
                        <th>FirstName</th>
                        <th>LastName</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($customers as $key => $value)
                    <tr>
                        <td align="center" ><a href="/customer/{{ $value->id }}" class="glyphicon glyphicon-arrow-right"></a></td>
                        <td>{{ $value->firstName }}</td>
                        <td>{{ $value->lastname }}</td>
                    </tr>
                    @endforeach
            </table>
        </div>
    </div>
</div>
@endSection